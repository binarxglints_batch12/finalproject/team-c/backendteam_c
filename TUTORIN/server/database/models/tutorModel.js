const { Schema, model } = require("mongoose");
const { courseSchema } = require("./courseModel");
const bcrypt = require("bcrypt");

const tutorSchema = new Schema(
  {
    fullname: {
      type: String,
    },
    email: {
      type: String,
    },
    password: {
      type: String,
      required: true,
      set: encryptPassword,
    },
    profile_pic: {
      type: String,
      default:
        "https://firebasestorage.googleapis.com/v0/b/tutorin-e5703.appspot.com/o/tutor_avatar.jpg?alt=media&token=c7f4627a-ee47-4bbc-8604-0da3cbe712ed",
    },
    category: {
      type: String,
    },
    level: {
      type: String,
    },
    skills: {
      type: String,
    },
    achievement: {
      type: String,
    },
    reviews: [
      {
        type: Schema.Types.ObjectId,
        ref: "Review",
      },
    ],
    about: {
      type: String,
    },
    course: [
      {
        type: Schema.Types.ObjectId,
        ref: "Course",
      },
    ],
    role: {
      type: String,
      default: "tutor",
    },
    rating: {
        type: Number,
        default: 1,
        required: true
    },
  },
  { timestamps: true }
);

//This will be running when user create or change password
function encryptPassword(password) {
  const encryptPassword = bcrypt.hashSync(password, 10);
  return encryptPassword;
}

const TutorModel = model("Tutor", tutorSchema);

module.exports = { TutorModel, tutorSchema };
