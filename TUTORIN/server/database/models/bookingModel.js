const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const bookingSchema = new Schema(
  {
    course_id: {
      type: Schema.Types.ObjectId,
      ref: "Course",
    },
    student_id: {
      type: Schema.Types.ObjectId,
      ref: "Student",
    },
    tutor_id: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: "Tutor",
    },
    date: {
      type: Date,
      required: true,
    },
    start_time: {
      required: true,
      type: String,
    },
    end_time: {
      required: true,
      type: String,
    },
    duration: {
      required: true,
      type: Number,
      required: true,
    },
    price: {
      required: true,
      type: Number,
    },
    payment_method: {
      required: true,
      type: String,
      enum: {
        values: ["Bank Transfer", "Debit/Credit Card", "E Wallet"],
        message: "{VALUE} is not supported",
      },
    },
    payment_status: {
      required: true,
      type: String,
      enum: ["Unpaid", "Paid"],
      default: "Unpaid",
    },
  },
  {
    timestamps: true,
  }
);

const BookingModel = mongoose.model("Booking", bookingSchema);

module.exports = { BookingModel, bookingSchema };
