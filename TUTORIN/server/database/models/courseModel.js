const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const courseSchema = new Schema(
    {
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        tutor_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Tutor",
            required: true
        },
        tutor_name: {
            type: String,
            required: true
        },
        category: {
            type: String,
            required: true,
            enum: {
                values: ["Music", "Design & Style", "Programming", "Language", "Lifestyle", "Math & Science"]
            }
        },
        subject: {
            type: String,
            required: true
        },
        level_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Level"
        },
        cover_img: {
            type: String,
            default: "https://firebasestorage.googleapis.com/v0/b/tutorin-a8bcf.appspot.com/o/Tutorin.jpg?alt=media&token=f1793b14-03ca-4afb-b6dd-7b88305546cd"
        },
        regular_price: {
            type: Number,
            required: true
        },
        premium_price: {
            type: Number
        },
        reviews: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Review"
            }
        ],
        rating: {
            type: Number,
            default: 0,
            required: true
        },
        isFavorites: {
            type: Boolean,
            default: false,
            required: true
        }
    },
    { timestamps: true }
)

const CourseModel = mongoose.model("Course", courseSchema);

module.exports = { CourseModel, courseSchema };