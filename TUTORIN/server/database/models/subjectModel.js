const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const subjectSchema = new Schema(
  {
    name: {
      type: String,
    },
    category_id: {
      type: Schema.Types.ObjectId,
      ref: "Category",
    },
  },
  {
    // Enable timestamps
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

const SubjectModel = mongoose.model("Subject", subjectSchema);

module.exports = { SubjectModel, subjectSchema };
