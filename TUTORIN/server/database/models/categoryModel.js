const mongoose = require("mongoose");
const { subjectSchema } = require("./subjectModel");

const Schema = mongoose.Schema;

const categorySchema = new Schema(
  {
    name: {
      type: String,
    },
    subject: [
      {
        type: Schema.Types.ObjectId,
        ref: "Subject",
      },
    ],
    course: [
      {
        type: Schema.Types.ObjectId,
        ref: "Course",
      },
    ],
  },
  {
    // Enable timestamps, it will auto create createdAt and updatedAt column to know when data has been created and updated
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

const categoryModel = mongoose.model("Category", categorySchema);

module.exports = { categoryModel, categorySchema };
