const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const skillSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        }
    },
    { timestamps: true }
);

const SkillModel = mongoose.model("Skill", skillSchema);

module.exports = { SkillModel };