const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const isFavoriteSchema = new Schema(
  {
    student_id: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: "Student",
    },
    course_id: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: "Course",
    },
  },
  {
    // Enable timestamps, it will auto create createdAt and updatedAt column to know when data has been created and updated
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

const isFavoriteModel = mongoose.model("isFavorite", isFavoriteSchema);

module.exports = { isFavoriteModel, isFavoriteSchema };
