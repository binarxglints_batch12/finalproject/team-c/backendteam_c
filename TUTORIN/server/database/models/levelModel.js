const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const levelSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        }
    },
    { timestamps: true }
);

const LevelModel = mongoose.model("Level", levelSchema);

module.exports = { LevelModel };