const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Schema = mongoose.Schema;

const studentSchema = new Schema(
  {
    fullname: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      // required: true,
      set: encryptPassword,
    },
    phone: {
      type: Number,
    },
    profile_pic: {
      type: String,
      default:
        "https://storage.googleapis.com/tutorin-e5703.appspot.com/avatar.jpg",
    },
    role: {
      type: String,
      default: "student",
    },
    favorite: [
      {
        type: Schema.Types.ObjectId,
        ref: "isFavorite",
      },
    ],
    googleId: {
      type: String,
    },
    facebookId: {
      type: String,
    },
  },
  { timestamps: true }
);

//This will be running when user create or change password
function encryptPassword(password) {
  const encryptPassword = bcrypt.hashSync(password, 10);
  return encryptPassword;
}

const StudentModel = mongoose.model("Student", studentSchema);

module.exports = { StudentModel, studentSchema };
