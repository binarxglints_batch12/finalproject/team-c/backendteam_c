const GoogleStrategy = require("passport-google-oauth20").Strategy;
const mongoose = require("mongoose");
const { StudentModel } = require("../models/studentModel");
const passport = require("passport");

// //Require dotenv
require("dotenv").config();

module.exports = (passport) => {
  passport.use(
    new GoogleStrategy(
      {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: "/api/v1/auth/google/callback",
      },
      async (accessToken, refreshToken, profile, done) => {
        const newStudent = {
          googleId: profile.id,
          fullname: profile.displayName,
          email: profile.emails[0].value,
          profile_pic: profile.photos[0].value,
        };

        // console.log(profile);
        try {
          student = await StudentModel.findOne({ googleId: profile.id }).select(
            "-__v -createdAt -updatedAt -password -favorite -profile_pic"
          );

          if (student) {
            done(null, student);
          } else {
            student = await StudentModel.create(newStudent);
            done(null, student);
          }
        } catch (err) {
          console.error(err);
        }
      }
    )
  );
  passport.serializeUser((student, done) => {
    done(null, student.id);
  });

  passport.deserializeUser((id, done) => {
    StudentModel.findById(id, (err, student) => done(err, student));
  });
};
