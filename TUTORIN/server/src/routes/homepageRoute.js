const express = require("express");
const router = express.Router();

const { getHome } = require("../controllers/homepageController");

router.route("/home").get(getHome);

module.exports = router;
