const route = require("express").Router();

const tutorAuth = require("../controllers/authTutorController");
const tutorMiddleware = require("../middleware/tutorMiddleware");
const tokenMiddleware = require("../middleware/tokenMiddleware");
const firebase = require("../middleware/firebase");

route.put(
  "/profile",
  tokenMiddleware.tokenAuth,
  tutorMiddleware.profileEdit,
  firebase.upload.single("profile_pic"),
  tutorAuth.editProfile
);
route.get("/", tokenMiddleware.tokenAuth, tutorAuth.getAllTutors);
route.get("/profile/:id", tokenMiddleware.tokenAuth, tutorAuth.getTutor);
route.post("/register", tutorMiddleware.register, tutorAuth.register);
route.post("/login", tutorMiddleware.login, tutorAuth.login);
route.get("/bestTutor", tutorAuth.getBestTutor);

module.exports = route;
