const express = require("express");
const router = express.Router();

const firebase = require("../middleware/firebase");

const {
  getCourse,
  getAllCourse,
  createCourse,
  updateCourse,
  deleteCourse,
} = require("../controllers/courseController");

router
  .route("/courses")
  .get(getAllCourse)
  .post(firebase.upload.single("cover_img"), createCourse);

router
  .route("/course/:id")
  .get(getCourse)
  .put(firebase.upload.single("cover_img"), updateCourse)
  .delete(deleteCourse);

module.exports = router;
