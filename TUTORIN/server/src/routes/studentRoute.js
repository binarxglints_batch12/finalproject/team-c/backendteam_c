const route = require("express").Router();

const studentController = require("../controllers/studentController");
const studentMiddleware = require("../middleware/studentMiddleware");
const tokenMiddleware = require("../middleware/tokenMiddleware");
const firebase = require("../middleware/firebase");
const { ensureAuth, ensureGuest } = require("../middleware/auth");

route.post("/register", studentMiddleware.register, studentController.register);

route.post("/login", studentMiddleware.login, studentController.login);

//  Login/Landing page in folder views/layout/login.hbs
route.get('/', ensureGuest,(req, res) => {
  res.render('login', {
    layout: 'login',
  })
})

// homepage page in folder views/homepage.hbs
route.get('/homepage', ensureAuth,(req, res) => {
  res.render("homepage")
})


route.get("/getAll", tokenMiddleware.tokenAuth, studentController.getAll);
route.get("/:id", tokenMiddleware.tokenAuth, studentController.getStudent);

route.put(
  "/profile",
  tokenMiddleware.tokenAuth,
  studentMiddleware.profileEdit,
  firebase.upload.single("profile_pic"),
  studentController.update
);

route.delete("/delete/:id", studentController.deleteById);

module.exports = route;
