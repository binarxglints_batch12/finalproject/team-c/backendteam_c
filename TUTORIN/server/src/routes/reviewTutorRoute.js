const express = require("express");
const router = express.Router();

const {
    createTutorReview,
    updateTutorReview,
    deleteTutorReview
} = require("../controllers/reviewTutorController");

router
    .route("/tutor/review/:id")
    .put(updateTutorReview)
    .delete(deleteTutorReview);

router
    .route("/tutor/review/:tutor_id")
    .post(createTutorReview);

module.exports = router;