const express = require("express");

const route = express.Router();

const {
  getAllSubjects,
  getSubject,
  createSubject,
  updateSubject,
  deleteSubject,
} = require("../controllers/subjectController");

//Get All Subjects
route.get("/", getAllSubjects);

//Get single Subject by id
route.get("/:id", getSubject);

//Create Subject
route.post("/", createSubject);

//Update Subject
route.put("/:id", updateSubject);

//Delete Subject
route.delete("/:id", deleteSubject);

module.exports = route;
