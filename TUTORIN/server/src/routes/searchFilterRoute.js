const express = require("express");
const router = express.Router();

const { 
    search, 
    categoryFilter,
    subjectFilter,
    ascFilter,
    descFilter,
    levelFilter
} = require("../controllers/searchFilterController");

router
    .route("/search")
    .get(search);

router
    .route("/course/category/name")
    .get(categoryFilter);

router
    .route("/course/subject/name")
    .get(subjectFilter);

router
    .route("/course/price/asc")
    .get(ascFilter);

router
    .route("/course/price/desc")
    .get(descFilter);

router
    .route("/course/level/:level_id")
    .get(levelFilter);

module.exports = router;