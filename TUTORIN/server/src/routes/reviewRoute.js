const express = require("express");
const router = express.Router();

const {
    getAllTutorReviews,
    createReview,
    updateReview,
    deleteReview
} = require("../controllers/reviewController");

router
    .route("/course/review/:id")
    .put(updateReview)
    .delete(deleteReview);

router
    .route("/course/review/:tutor_id")
    .get(getAllTutorReviews)
    .post(createReview);

module.exports = router;