const express = require("express");

const route = express.Router();

const tokenMiddleware = require("../middleware/tokenMiddleware");
const {
  createFavorite,
  getAllFavorite,
  deleteFavorite,
  getAllFavoritByStudentId,
  deleteFavoriteByCourseId,
} = require("../controllers/isFavoriteController");

//Get All Favourite
route.get("/", tokenMiddleware.tokenAuth, getAllFavorite);

// //Get All Favourite by student id
route.get("/student", tokenMiddleware.tokenAuth, getAllFavoritByStudentId);

//Create Favourite
route.post("/", tokenMiddleware.tokenAuth, createFavorite);

// //Update Favourite
// route.put("/:id", updateSubject);

//Delete Favorite by id
route.delete("/:id", tokenMiddleware.tokenAuth, deleteFavorite);

//Delete Favorite by course_id
route.delete(
  "/course/:id",
  tokenMiddleware.tokenAuth,
  deleteFavoriteByCourseId
);

module.exports = route;
