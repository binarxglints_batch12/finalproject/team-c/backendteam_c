const route = require("express").Router();
const passport = require("passport");

// @desc    Auth with Google
// @route   GET /auth/google
route.get(
  "/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);

// @desc    Google auth callback
// @route   GET /auth/google/callback
route.get(
  "/google/callback",
  passport.authenticate("google", {
    //if you fail to sign up with login, you will be navigate to this route
    failureRedirect: "/",
  }),
  (req, res) => {
    //if you success to sign up with google, you will be navigate to this route
    console.log("Token", req.user);
    res.redirect("/api/v1/student/homepage");
  }
);

// @desc    Logout user
// @route   /auth/logout
route.get("/logout", (req, res) => {
  req.logout();
  res.redirect("/api/v1/student/homepage");
});

module.exports = route;
