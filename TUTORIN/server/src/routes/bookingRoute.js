const express = require("express");
const route = express.Router();

const {
  getAllBooking,
  getBooking,
  createBooking,
  updateBooking,
  deleteBooking,
  getAllBookingByStudentId,
  getAllBookingByTutorId,
} = require("../controllers/bookingController");
const tokenMiddleware = require("../middleware/tokenMiddleware");

//Get All Bookings
route.get("/", tokenMiddleware.tokenAuth, getAllBooking);

//Get All By Student Id
route.get("/student", tokenMiddleware.tokenAuth, getAllBookingByStudentId);

//Get All By Tutor Id
route.get("/tutor", tokenMiddleware.tokenAuth, getAllBookingByTutorId);

//Get single booking by id
route.get("/:id", tokenMiddleware.tokenAuth, getBooking);

//Create booking
route.post("/", tokenMiddleware.tokenAuth, createBooking);

//Update booking
route.put("/:id", tokenMiddleware.tokenAuth, updateBooking);

//Delete booking
route.delete("/:id", tokenMiddleware.tokenAuth, deleteBooking);

module.exports = route;
