const express = require("express");
const router = express.Router();

const {
    getLevel,
    getAllLevel,
    createLevel,
    updateLevel,
    deleteLevel
} = require("../controllers/levelController");

router
    .route("/level")
    .get(getAllLevel)
    .post(createLevel);

router
    .route("/level/:id")
    .get(getLevel)
    .put(updateLevel)
    .delete(deleteLevel);

module.exports = router;