const express = require("express");
const router = express.Router();

const {
    getSkill,
    getAllSKill,
    createSkill,
    updateSkill,
    deleteSkill
} = require("../controllers/skillController");

router
    .route("/skill")
    .get(getAllSKill)
    .post(createSkill);

router
    .route("/skill/:id")
    .get(getSkill)
    .put(updateSkill)
    .delete(deleteSkill);

module.exports = router;