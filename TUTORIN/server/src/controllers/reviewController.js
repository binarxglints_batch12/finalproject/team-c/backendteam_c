const { ReviewModel } = require("../../database/models/reviewModel");
const { StudentModel } = require("../../database/models/studentModel");
const { CourseModel } = require("../../database/models/courseModel");
const { TutorModel } = require("../../database/models/tutorModel");

exports.getAllTutorReviews = async (req, res) => {
  try {
    const { tutor_id } = req.params;
    const courseReviews = await ReviewModel.find({ tutor_id })
      .select("-tutor_id -__v")
      .populate("student_id", "fullname profile_pic");

    if (courseReviews) {
      const page = parseInt(req.query.page);
      const limit = 5;
      const startIndex = (page - 1) * limit;
      const endIndex = page * limit;

      const result = {};

      if (endIndex < courseReviews.length) {
        result.next = {
          page: page + 1,
        };
      }

      if (startIndex > 0) {
        result.previous = {
          page: page - 1,
        };
      }

      result.results = courseReviews.slice(startIndex, endIndex);

      const resPayload = {
        statusCode: 200,
        statusText: "success",
        message: "Get all course reviews",
        data: [ page, result ],
      };
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

exports.createReview = async (req, res) => {
  try {
    const { tutor_id } = req.params;
    const { student_id, rating, reviews } = req.body;

    if (!req.body) {
      return res
        .json({
          statusCode: 400,
          statusText: "Not Found",
          message: "Content can not be empty",
        })
        .status(400);
    } else {
      const createReview = await ReviewModel.create({ ...req.body });

      const result = await ReviewModel.findById(createReview._id)
        .populate("tutor_id")
        .populate("student_id")
        .exec();

      const data = {
        fullname: result.student_id.fullname,
        profile_pic: result.student_id.profile_pic,
        rating: result.rating,
        reviews: result.reviews,
        created_at: result.createdAt,
      };

      const updateTutor = await TutorModel.findByIdAndUpdate(tutor_id, {
        $push: { reviews: createReview._id },
      });

      const findAllReview = await ReviewModel.find({ tutor_id });
      const allRating = findAllReview.map((x) => x.rating);
      const avgRating = allRating.reduce((a, b) => a + b) / allRating.length;

      const updateRating = await TutorModel.findByIdAndUpdate(tutor_id, {
        rating: avgRating
      });

      const resPayload = {
        statusCode: 200,
        statusText: "success",
        message: "Create a review",
        data,
      };
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

exports.updateReview = async (req, res) => {
  try {
    if (!req.body) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Data to update can not be empty",
        })
        .status(404);
    }

    const id = req.params.id;
    const update = await ReviewModel.findByIdAndUpdate(id, req.body);

    const findReview = await ReviewModel.find({ tutor_id: update.tutor_id });
    const allRating = findReview.map((x) => x.rating);
    const avgRating = allRating.reduce((a, b) => a + b) / allRating.length;

    const updateRating = await CourseModel.findByIdAndUpdate(update.tutor_id, {
      rating: avgRating,
    });

    const result = await ReviewModel.findById(update._id)
      .populate("course_id")
      .populate("student_id");

    const data = {
      username: result.student_id.username,
      fullname: result.student_id.fullname,
      profile_pic: result.student_id.profile_pic,
    };

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Review successfully updated",
      data,
    };

    if (!update) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: `Cannot update review with id ${id}`,
        })
        .status(404);
    } else {
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

exports.deleteReview = async (req, res) => {
  try {
    const id = req.params.id;
    const findReview = await ReviewModel.findOne({ _id: id });
    const tutor_id = findReview.tutor_id;
    const deleteReview = await ReviewModel.findByIdAndDelete(id);

    const findAllReview = await ReviewModel.find({ tutor_id });

    const allRating = findReview.map((x) => x.rating);
    const avgRating = allRating.reduce((a, b) => a + b) / allRating.length;

    const updateRating = await CourseModel.findByIdAndUpdate(tutor_id, {
      rating: avgRating,
    });

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Review succesfully deleted",
    };
    res.json(resPayload).status(200);
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};
