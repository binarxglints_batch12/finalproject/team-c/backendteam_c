require("dotenv").config();

const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { TutorModel } = require("../../database/models/tutorModel");
const { BookingModel } = require("../../database/models/bookingModel");
const firebase = require("../middleware/firebase");

const login = async (req, res) => {
  const { email, password } = req.body;
  try {
    const findTutor = await TutorModel.findOne({
      email: email,
    });
    if (!findTutor) {
      res.status(400).send({
        status: 404,
        message: {
          email: "email doesn't exist",
        },
        data: [],
      });
    } else {
      const isPasswordCorrect = await bcrypt.compare(
        password,
        findTutor.password
      );
      console.log(isPasswordCorrect);
      if (isPasswordCorrect) {
        const payload = {
          id: findTutor.id,
          fullname: findTutor.fullname,
          email: findTutor.email,
          role: findTutor.role,
        };
        jwt.sign(
          payload,
          process.env.TOKEN_SECRET,
          {
            expiresIn: "30d",
          },
          (err, token) => {
            res.send({
              status: 200,
              data: findTutor,
              token: "Bearer token: " + token,
            });
          }
        );
      } else {
        res.status(401).send({
          status: 401,
          message: {
            password: "password didn't match",
          },
          data: [],
        });
      }
    }
  } catch (err) {
    res.status(400).send({
      status: 400,
      message: err,
      data: [],
    });
  }
};

const register = async (req, res) => {
  try {
    const findTutor = await TutorModel.findOne({
      email: req.body.email,
    });
    if (findTutor) {
      res.status(409).send({
        status: 409,
        message: {
          email: "Email is already taken",
        },
      });
    } else {
      const registerTutor = await TutorModel.create({ ...req.body });
      const payload = {
        id: registerTutor._id,
        fullname: registerTutor.fullname,
        email: registerTutor.email,
      };
      jwt.sign(
        payload,
        process.env.TOKEN_SECRET,
        {
          expiresIn: "30d",
        },
        (err, token) => {
          res.send({
            status: 200,
            data: registerTutor,
            token: "Bearer token: " + token,
          });
        }
      );
    }
  } catch (err) {
    console.log(err);
    res.status(400).send({
      status: 400,
      message: err,
      data: [],
    });
  }
};

const editProfile = async (req, res) => {
  const data = await TutorModel.findOne({ _id: req.user.id });
  const newData = req.body;

  if (!data) {
    res.status(404).send({
      status: 404,
      message: "User id Not Found",
    });
  }

  let imageURL = data.profile_pic;

  if (req.file) {
    //Upload Image File to firebase
    const fileName = new Date().toISOString() + "-" + req.file.originalname;
    imageURL =
      "https://storage.googleapis.com/tutorin-317818.appspot.com/" + fileName;

    const blob = firebase.bucket.file(fileName);
    const blobWriter = blob.createWriteStream({
      metadata: {
        contentType: req.file.mimetype,
      },
    });

    blobWriter.on("error", (err) => {
      if (err) throw err;
    });
    blobWriter.on("finish", (err) => {
      if (err) throw err;
    });
    blobWriter.end(req.file.buffer);
  }

  try {
    newData.profile_pic = imageURL;
    const updatedUser = await TutorModel.findOneAndUpdate(
      { _id: req.user.id },
      newData,
      {
        new: true,
      }
    );

    return res.send({
      statusCode: 200,
      statusText: "success",
      data: updatedUser,
    });
  } catch (err) {
    console.log(err);
    res.status(400).send({
      status: 400,
      message: err,
      data: [],
    });
  }
};

//Get all Tutors
const getAllTutors = async (req, res) => {
  //Pagination
  const page = parseInt(req.query.page);
  const limit = parseInt(req.query.limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const data = {};

  if (endIndex < (await TutorModel.countDocuments().exec())) {
    data.next = {
      page: page + 1,
      limit: limit,
    };
  }

  if (startIndex > 0) {
    data.previous = {
      page: page - 1,
      limit: limit,
    };
  }
  try {
    data.total_data = await TutorModel.countDocuments().exec();
    data.results = await TutorModel.find().limit(limit).skip(startIndex).exec();

    res.json({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.json({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a Tutor by ID
const getTutor = async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await TutorModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//GET Top 4 Best Tutor
const getBestTutor = async (req, res) => {
  try {
    const bestTutor = await BookingModel.aggregate([
      { $group: { _id: "$tutor_id", value: { $sum: 1 } } },
      { $sort: { value: -1 } },
      { $limit: 4 },
    ]);

    const bestTutorProfile = await TutorModel.find({
      _id: {
        $in: [
          mongoose.Types.ObjectId(bestTutor[0]._id),
          mongoose.Types.ObjectId(bestTutor[1]._id),
          mongoose.Types.ObjectId(bestTutor[2]._id),
          mongoose.Types.ObjectId(bestTutor[3]._id),
        ],
      },
    })
      .select(" -reviews -password -createdAt -updatedAt -__v")
      .populate({
        path: "course",
        select: "-createdAt -updatedAt -__v",
        perDocumentLimit: 1,
      })
      .exec();
    res.send({
      statusCode: 200,
      statusText: "success",
      data: bestTutorProfile,
    });
  } catch (error) {
    console.log(error);
    res.json({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

module.exports = {
  register,
  login,
  editProfile,
  getTutor,
  getAllTutors,
  getBestTutor,
};
