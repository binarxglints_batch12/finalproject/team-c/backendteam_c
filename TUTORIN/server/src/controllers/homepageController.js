const { CourseModel } = require("../../database/models/courseModel");
const { categoryModel } = require("../../database/models/categoryModel");

exports.getHome = async (req, res) => {
  try {
    const courses = await CourseModel.find()
      .limit(3)
      .select("-reviews -__v -createdAt -updatedAt -level_id")
      .populate("tutor_id", "fullname");

    const category = await categoryModel.find().limit(7).select("name");

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Homepage",
      data: {
        courses: courses,
        category: category,
      },
    };

    res.send(resPayload).status(200);
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};
