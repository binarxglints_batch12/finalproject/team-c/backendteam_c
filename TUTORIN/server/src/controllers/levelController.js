const { LevelModel } = require("../../database/models/levelModel");

exports.getLevel = async (req, res) => {
    try {
        const id = req.params.id;

        const level = await LevelModel.findById(id);

        if (!level) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Level is not found",
            })
            .status(404);
        } else {
            res.json({
                statusCode: 200,
                statusText: "success",
                message: "Level is found",
                data: level
            })
            .status(200);
        }

    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};

exports.getAllLevel = async (req, res) => {
    try {
        const levels = await LevelModel.find()
            .select("-__v");

        if (!levels) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Level is not found",
            })
            .status(404);
        } else {
            res.json({
                statusCode: 200,
                statusText: "success",
                message: "Levels is found",
                data: levels
            })
            .status(200);
        }
    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};

exports.createLevel = async (req, res) => {
    try {
        const newLevel = await LevelModel.create(req.body);

        if (!newLevel) {
            return res.json({
                statusCode: 400,
                statusText: "Not Found",
                message: "Content can not be empty"
            })
            .status(400);
        } else {
            res.json({
                statusCode: 201,
                statusText: "Created",
                message: "Level is created",
                data: newLevel
            })
            .status(201);
        }

    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};

exports.updateLevel = async (req, res) => {
    try {
        if (!req.body) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Data to update can not be empty"
            })
            .status(404);
        };

        const id = req.params.id;
        const update = await LevelModel.findByIdAndUpdate(id, req.body, {
            useFindAndModify: false
        });

        if (!update) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Cannot update Level. Maybe level was not found"
            })
            .status(404);
        } else {
            res.json({
                statusCode: 200,
                statusText: "success",
                message: "Level successfully updated",
                data: update
            })
            .status(200);
        }
        
    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};

exports.deleteLevel = async (req, res) => {
    try {
        const remove = await LevelModel.findByIdAndRemove(req.params.id);

        if (!remove) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Cannot delete level"
            })
            .status(404);
        } else {
            res.json({
                statusCode: 204,
                statusText: "No Content",
                message: "Level is deleted successfully"
            })
            .status(204);
        }

    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};