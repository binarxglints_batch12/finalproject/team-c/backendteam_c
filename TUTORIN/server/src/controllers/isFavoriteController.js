const mongoose = require("mongoose");
const { isFavoriteModel } = require("../../database/models/isFavoriteModel");
const { StudentModel } = require("../../database/models/studentModel");
const { CourseModel } = require("../../database/models/courseModel");

//Create favorite
exports.createFavorite = async (req, res) => {
  const course_id = req.body.course_id;
  try {
    const dataFavorite = await isFavoriteModel.find({
      $and: [{ student_id: req.user.id, course_id }],
    });

    const dataCourse = await CourseModel.find({ _id: course_id });

    //check if course_id is exist
    if (dataCourse.length === 0) {
      return res.status(404).json({
        statusCode: 404,
        message: "Fail.",
        statusText: "This Course is not exist",
      });

      //Check This Course has been added to favorite ?
    } else if (dataFavorite.length === 1) {
      return res.status(409).json({
        statusCode: 409,
        message: "Fail.",
        statusText: "This course already in your favorite",
      });
    } else {
      const data = await new isFavoriteModel({
        student_id: req.user.id,
        course_id: req.body.course_id,
      });
      //push favorite_id to model student
      const student = await StudentModel.findById({ _id: req.user.id });
      await student.favorite.push(data);
      await student.save();
      await data.save();
      res.status(201).json({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      statusCode: 500,
      message: "Fail",
      statusText: error.message,
    });
  }
};

exports.getAllFavorite = async (req, res) => {
  //Pagination
  const page = parseInt(req.query.page);
  const limit = parseInt(req.query.limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const data = {};

  if (endIndex < (await isFavoriteModel.countDocuments().exec())) {
    data.next = {
      page: page + 1,
      limit: limit,
    };
  }

  if (startIndex > 0) {
    data.previous = {
      page: page - 1,
      limit: limit,
    };
  }

  try {
    data.total_data = await isFavoriteModel.countDocuments().exec();
    data.results = await isFavoriteModel
      .find()
      .select("-createdAt -updatedAt -__v")
      .populate("student_id", "fullname")
      .populate({
        path: "course_id",
        populate: { path: "tutor_id", select: "fullname" },
        select: [
          "description",
          "tutor_id",
          "category",
          "level_id",
          "cover_img",
          "regular_price",
          "premium_price",
          "reviews",
          "rating",
          "subject",
        ],
      })
      .limit(limit)
      .skip(startIndex)
      .exec();

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Get all Favourite",
      data,
    };
    res.json(resPayload).status(200);
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

//GET All Favorit By Student id token
exports.getAllFavoritByStudentId = async (req, res) => {
  const student_id = req.user.id;

  if (!mongoose.Types.ObjectId.isValid(student_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await isFavoriteModel
      .find({ student_id })
      .select("-__v -createdAt -updatedAt")
      .populate({
        path: "student_id",
        select: "-password -createdAt -updatedAt -favorite -__v",
      })
      .populate({ path: "course_id", select: "-createdAt -updatedAt -__v" });

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Delete Favorite by id
exports.deleteFavorite = async (req, res) => {
  const course_id = req.params.id;
  const student_id = req.user.id;

  if (!mongoose.Types.ObjectId.isValid(course_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await isFavoriteModel.find({
      $and: [{ student_id }, { course_id }],
    });
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await StudentModel.findOneAndUpdate(
        { _id: data.student_id },
        { $pullAll: { favorite: [data._id] } },
        { new: true }
      );
      await isFavoriteModel.findByIdAndRemove(data._id);
      return res.send({
        statusCode: 500,
        statusText: "Success",
        statusMessage: "Remove from Favorite",
      });
    }
  }
};

//Delete Favorite by Student_id and course_id
exports.deleteFavoriteByCourseId = async (req, res) => {
  const course_id = req.params.id;
  const student_id = req.user.id;

  if (!mongoose.Types.ObjectId.isValid(course_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await isFavoriteModel.find({
      $and: [{ course_id, student_id }],
    });
    if (data.length === 0) {
      return res.status(404).send({
        statusCode: 404,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await StudentModel.findOneAndUpdate(
        { _id: student_id },
        { $pullAll: { favorite: [data[0]._id] } },
        { new: true }
      );
      await isFavoriteModel.deleteOne({ _id: data[0]._id });
      return res.send({
        statusCode: 500,
        statusText: "Success",
        statusMessage: "Remove from Favorite",
      });
    }
  }
};
