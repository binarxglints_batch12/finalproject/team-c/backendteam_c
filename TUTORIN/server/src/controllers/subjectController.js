//require model
const mongoose = require("mongoose");
const { SubjectModel } = require("../../database/models/subjectModel");
const { categoryModel } = require("../../database/models/categoryModel");

//Get all Subjects
exports.getAllSubjects = async (req, res) => {
  //Pagination
  const page = parseInt(req.query.page);
  const limit = parseInt(req.query.limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const data = {};

  if (endIndex < (await SubjectModel.countDocuments().exec())) {
    data.next = {
      page: page + 1,
      limit: limit,
    };
  }

  if (startIndex > 0) {
    data.previous = {
      page: page - 1,
      limit: limit,
    };
  }
  try {
    data.total_data = await SubjectModel.countDocuments().exec();
    data.results = await SubjectModel.find()
      .select("-__v -createdAt -updatedAt")
      .populate("category_id", "name")
      .limit(limit)
      .skip(startIndex)
      .exec();

    res.json({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.json({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//Get a Subject by ID
exports.getSubject = async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await SubjectModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create Subject
exports.createSubject = async (req, res) => {
  const data = await SubjectModel(req.body);
  //push subject_id to model category
  const category = await categoryModel.findById({ _id: data.category_id });
  try {
    await category.subject.push(data);
    await category.save();
    await data.save();
    res.status(201).json({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.status(409).json({
      statusCode: 409,
      message: "Fail.",
      statusText: "error",
    });
  }
};

//Update Subject
exports.updateSubject = async (req, res) => {
  const _id = req.params.id;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await SubjectModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedSubject = await SubjectModel.findByIdAndUpdate(
        _id,
        newData,
        {
          new: true,
        }
      );
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedSubject,
      });
    }
  }
};

//Delete Subject by ID
exports.deleteSubject = async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await SubjectModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await SubjectModel.findByIdAndRemove(_id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Subject deleted succesfully",
      });
    }
  }
};
