//require model
const mongoose = require("mongoose");
const moment = require("moment");
const { BookingModel } = require("../../database/models/bookingModel");

//Get all booking
exports.getAllBooking = async (req, res) => {
  //Pagination
  const page = parseInt(req.query.page);
  const limit = parseInt(req.query.limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const data = {};

  if (endIndex < (await BookingModel.countDocuments().exec())) {
    data.next = {
      page: page + 1,
      limit: limit,
    };
  }

  if (startIndex > 0) {
    data.previous = {
      page: page - 1,
      limit: limit,
    };
  }
  try {
    data.total_data = await BookingModel.countDocuments().exec();
    data.results = await BookingModel.find()
      .limit(limit)
      .skip(startIndex)
      .exec();

    res.json({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.json({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error.message,
    });
  }
};

//Get All Booking by student id
exports.getAllBookingByStudentId = async (req, res) => {
  try {
    const student_id = req.user.id;
    const data = await BookingModel.find({ student_id: student_id })
      .populate({
        path: "course_id",
        select: "-__v -createdAt -updatedAt",
      })
      .populate({
        path: "tutor_id",
        select: "-__v -createdAt -updatedAt -password -reviews",
      })
      .populate({
        path: "student_id",
        select: "-__v -createdAt -updatedAt -favorite -password",
      });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      message: "Fail",
      statusText: error.message,
    });
  }
};

//Get All Booking by tutor id
exports.getAllBookingByTutorId = async (req, res) => {
  try {
    const tutor_id = req.user.id;
    const data = await BookingModel.find({ tutor_id })
      .populate({
        path: "course_id",
        select: "-__v -createdAt -updatedAt",
      })
      .populate({
        path: "tutor_id",
        select: "-__v -createdAt -updatedAt -password -reviews",
      })
      .populate({
        path: "student_id",
        select: "-__v -createdAt -updatedAt -favorite -password",
      });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      message: "Fail",
      statusText: error.message,
    });
  }
};

//Get a Booking by ID
exports.getBooking = async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await BookingModel.findById(_id);

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

//Create Booking
exports.createBooking = async (req, res) => {
  //Check Scheduled Booking Existing
  const tutorSchedule = await BookingModel.find({
    $and: [
      {
        tutor_id: req.body.tutor_id,
      },
      {
        date: req.body.date,
      },
    ],
  });
  const format = "hh:mm";
  const start_time = moment(req.body.start_time, format);
  const bookedStartTime = moment(tutorSchedule.start_time, format);
  const bookedEndTime = moment(tutorSchedule.end_time, format);

  if (start_time.isBetween(bookedStartTime, bookedEndTime)) {
    res.status(400).json({
      statusCode: 400,
      statusText: "Fail",
      message: "Tutor has been booked at this time.",
    });
  }

  try {
    const data = await BookingModel(req.body);
    if (req.body.duration < 1) {
      res.status(400).json({
        statusCode: 400,
        message: "Fail",
        statusText: "Duration minimum 1 Hour",
      });
    } else {
      await data.save();
      res.status(200).json({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  } catch (error) {
    res.status(400).json({
      statusCode: 400,
      message: "Fail.",
      statusText: "error",
    });
  }
};

//Update Booking
exports.updateBooking = async (req, res) => {
  const _id = req.params.id;
  const newData = req.body;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await BookingModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      const updatedBooking = await BookingModel.findByIdAndUpdate(
        _id,
        newData,
        {
          new: true,
        }
      );
      return res.send({
        statusCode: 200,
        statusText: "success",
        data: updatedBooking,
      });
    }
  }
};

//Delete Subject by ID
exports.deleteBooking = async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await BookingModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await BookingModel.findByIdAndRemove(_id);
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Subject deleted succesfully",
      });
    }
  }
};
