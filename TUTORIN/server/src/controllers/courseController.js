const { categoryModel } = require("../../database/models/categoryModel");
const { CourseModel } = require("../../database/models/courseModel");
const { ReviewModel } = require("../../database/models/reviewModel");
const { TutorModel } = require("../../database/models/tutorModel");
const mongoose = require("mongoose");

const firebase = require("../middleware/firebase");

exports.getCourse = async (req, res) => {
  try {
    const id = req.params.id;
    const course = await CourseModel.findById(id)
      .select("-reviews -__v -createdAt -updatedAt")
      .populate("tutor_id", "-email -password -__v")
      .populate("level_id", "-_id -createdAt -updatedAt -__v");

    const random = Math.floor(Math.random() * 5);
    const courses = await CourseModel.find()
      .skip(random)
      .limit(3)
      .select("-reviews -__v -createdAt -updatedAt -level_id")
      .populate("tutor_id", "fullname");

    const { tutor_id } = req.body;
    const courseReviews = await ReviewModel.find({ tutor_id })
      .select("_id student_id rating reviews createdAt updatedAt")
      .populate("student_id", "username profile_pic");

    const countReview = {};
    countReview.total_review = await ReviewModel.countDocuments({
      tutor_id,
    }).exec();

    if (courseReviews) {
      const page = parseInt(req.query.page);
      const limit = 5;
      const startIndex = (page - 1) * limit;
      const endIndex = page * limit;

      const result = {};

      if (endIndex < courseReviews.length) {
        result.next = {
          page: page + 1,
        };
      }

      if (startIndex > 0) {
        result.previous = {
          page: page - 1,
        };
      }

      result.results = courseReviews.slice(startIndex, endIndex);

      const resPayload = {
        statusCode: 200,
        statusText: "success",
        message: "Course is found",
        data: course,
        countReview,
        page,
        result,
        courses,
      };

      if (!course) {
        return res
          .json({
            statusCode: 404,
            statusText: "Not Found",
            message: "Course is not found",
          })
          .status(404);
      } else {
        res.json(resPayload).status(404);
      }
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

exports.getAllCourse = async (req, res) => {
  try {
    const courses = await CourseModel.find()
      .select("-reviews -__v -createdAt -updatedAt -level_id")
      .populate("tutor_id", "fullname");

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Get all courses",
      data: courses,
    };
    res.json(resPayload).status(200);
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

exports.createCourse = async (req, res) => {
  try {
    const { tutor_id, category } = req.body;
    if (!req.body) {
      return res
        .json({
          statusCode: 400,
          statusText: "Not Found",
          message: "Content can not be empty",
        })
        .status(400);
    }

    let cover_img = CourseModel.cover_img;

    if (req.file) {
      const fileName = new Date().toISOString() + "-" + req.file.originalname;
      cover_img =
        "https://storage.googleapis.com/tutorin-317818.appspot.com/" + fileName;

      const blob = firebase.bucket.file(fileName);
      const blobWriter = blob.createWriteStream({
        metadata: {
          contentType: req.file.mimetype,
        },
      });

      blobWriter.on("error", (err) => {
        if (err) throw err;
      });
      blobWriter.on("finish", (err) => {
        if (err) throw err;
      });
      blobWriter.end(req.file.buffer);
    }

    const newCourse = await CourseModel.create({ ...req.body, cover_img });

    const updateTutor = await TutorModel.findByIdAndUpdate(tutor_id, {
      $push: { course: newCourse._id },
    });

    res
      .json({
        statusCode: 201,
        statusText: "Created",
        message: "Course is created",
        data: newCourse,
      })
      .status(201);
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

exports.updateCourse = async (req, res) => {
  try {
    if (!req.body) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Data to update can not be empty",
        })
        .status(404);
    }

    let cover_img = CourseModel.cover_img;

    if (req.file) {
      const fileName = new Date().toISOString() + "-" + req.file.originalname;
      cover_img =
        "https://storage.googleapis.com/tutorin-317818.appspot.com/" + fileName;

      const blob = firebase.bucket.file(fileName);
      const blobWriter = blob.createWriteStream({
        metadata: {
          contentType: req.file.mimetype,
        },
      });

      blobWriter.on("error", (err) => {
        if (err) throw err;
      });
      blobWriter.on("finish", (err) => {
        if (err) throw err;
      });
      blobWriter.end(req.file.buffer);
    }

    const id = req.params.id;
    const update = await CourseModel.findByIdAndUpdate(
      id,
      { ...req.body, cover_img },
      {
        useFindAndModify: false,
      }
    );

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Course successfully updated",
      data: update,
    };

    if (!update) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Cannot update course. Maybe course was not found",
        })
        .status(404);
    } else {
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

exports.deleteCourse = async (req, res) => {
  try {
    const remove = await CourseModel.findByIdAndRemove(req.params.id);

    if (!remove) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Cannot delete course",
        })
        .status(404);
    } else {
      return res
        .json({
          statusCode: 204,
          statusText: "No Content",
          message: "Course is deleted successfully",
        })
        .status(204);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(500);
  }
};

exports.getCourseByTutorId = async (req, res) => {
  try {
    const tutor_id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(tutor_id)) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Format Id invalid",
      });
    } else {
      const data = await CourseModel.find({ tutor_id });

      if (!data) {
        return res.send({
          statusCode: 500,
          statusText: "fail",
          statusMessage: "Id not found",
        });
      } else {
        res.send({
          statusCode: 200,
          statusText: "success",
          data,
        });
      }
    }
  } catch (error) {
    return res.status(500).json({
      statusCode: 500,
      statusText: "error",
      message: error.message,
    });
  }
};
