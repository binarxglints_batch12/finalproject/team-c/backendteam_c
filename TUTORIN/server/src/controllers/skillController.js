const { SkillModel } = require("../../database/models/skillModel");

exports.getSkill = async (req, res) => {
    try {
        const id = req.params.id;

        const skill = await SkillModel.findById(id);

        if (!skill) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Skill is not found",
            })
            .status(404);
        } else {
            res.json({
                statusCode: 200,
                statusText: "success",
                message: "Skill is found",
                data: skill
            })
            .status(200);
        }

    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};

exports.getAllSKill = async (req, res) => {
    try {
        const skills = await SkillModel.find()
            .select("-__v");

        if (!skills) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Skill is not found",
            })
            .status(404);
        } else {
            res.json({
                statusCode: 200,
                statusText: "success",
                message: "Skills is found",
                data: skills
            })
            .status(200);
        }
    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};

exports.createSkill = async (req, res) => {
    try {
        const newSkill = await SkillModel.create(req.body);

        if (!newSkill) {
            return res.json({
                statusCode: 400,
                statusText: "Not Found",
                message: "Content can not be empty"
            })
            .status(400);
        } else {
            res.json({
                statusCode: 201,
                statusText: "Created",
                message: "Skill is created",
                data: newSkill
            })
            .status(201);
        }

    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};

exports.updateSkill = async (req, res) => {
    try {
        if (!req.body) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Data to update can not be empty"
            })
            .status(404);
        };

        const id = req.params.id;
        const update = await SkillModel.findByIdAndUpdate(id, req.body, {
            useFindAndModify: false
        });

        if (!update) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Cannot update skill. Maybe skill was not found"
            })
            .status(404);
        } else {
            res.json({
                statusCode: 200,
                statusText: "success",
                message: "Skill successfully updated",
                data: update
            })
            .status(200);
        }
        
    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};

exports.deleteSkill = async (req, res) => {
    try {
        const remove = await SkillModel.findByIdAndRemove(req.params.id);

        if (!remove) {
            return res.json({
                statusCode: 404,
                statusText: "Not Found",
                message: "Cannot delete skill"
            })
            .status(404);
        } else {
            res.json({
                statusCode: 204,
                statusText: "No Content",
                message: "Skill is deleted successfully"
            })
            .status(204);
        }

    } catch (error) {
        res.json({
            statusCode: 500,
            statusText: "error",
            message: error.message
        })
        .status(500);
    }
};