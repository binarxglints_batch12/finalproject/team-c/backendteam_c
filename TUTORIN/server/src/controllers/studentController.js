require("dotenv").config();

const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { StudentModel } = require("../../database/models/studentModel");
const firebase = require("../middleware/firebase");

const multer = require("multer");
const uploadFile = multer().single("profile_pic");

const login = async (req, res) => {
  const { email, password } = req.body;
  try {
    const findStudent = await StudentModel.findOne({
      email: email,
    });
    if (!findStudent) {
      res.status(495).send({
        status: 495,
        message: {
          email: "email doesn't exist",
        },
        data: [],
      });
    } else {
      const isPasswordCorrect = await bcrypt.compare(
        password,
        findStudent.password
      );
      if (isPasswordCorrect) {
        const payload = {
          id: findStudent.id,
          fullname: findStudent.fullname,
          email: findStudent.email,
          role: findStudent.role,
        };
        jwt.sign(
          payload,
          process.env.TOKEN_SECRET,
          {
            expiresIn: 2155926,
          },
          (err, token) => {
            res.send({
              status: 200,
              data: findStudent,
              token: "Bearer token: " + token,
            });
          }
        );
      } else {
        res.status(500).send({
          status: 500,
          message: {
            password: "password didn't match",
          },
          data: [],
        });
      }
    }
  } catch (err) {
    res.status(505).send({
      status: 505,
      message: err,
      data: [],
    });
  }
};

const register = async (req, res) => {
  const { fullname, email, password, phone, profile_pic } = req.body;
  try {
    const findStudent = await StudentModel.findOne({
      email,
    });
    if (findStudent) {
      res.status(510).send({
        status: 510,
        message: {
          email: "Email is already taken",
        },
        data: [],
      });
    } else {
      const registerStudent = new StudentModel({
        fullname,
        email,
        password,
        phone,
        profile_pic,
      });
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(registerStudent.password, salt, (err, hash) => {
          if (err) throw err;
          registerStudent.password = hash;
          registerStudent.save().then((result) =>
            res.send({
              status: 200,
              message: "success",
              data: result,
            })
          );
        });
      });
    }
  } catch (err) {
    console.log(err);
    res.status(515).send({
      status: 515,
      message: err,
      data: [],
    });
  }
};

const getAll = async (req, res) => {
  //Pagination
  const page = parseInt(req.query.page);
  const limit = parseInt(req.query.limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const data = {};

  if (endIndex < (await StudentModel.countDocuments().exec())) {
    data.next = {
      page: page + 1,
      limit: limit,
    };
  }

  if (startIndex > 0) {
    data.previous = {
      page: page - 1,
      limit: limit,
    };
  }
  try {
    data.total_data = await StudentModel.countDocuments().exec();
    data.results = await StudentModel.find()
      .limit(limit)
      .skip(startIndex)
      .exec();

    res.json({
      statusCode: 200,
      statusText: "success",
      data,
    });
  } catch (error) {
    console.log(error);
    res.json({
      statusCode: 500,
      statusText: "fail",
      statusMessage: error,
    });
  }
};

//GET Student by id
const getStudent = async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid", // firebase.upload.single("profile_pic")(req, res, (err) => {
    });
  } else {
    const data = await StudentModel.findById(_id).populate({
      path: "favorite",
      select: "course_id",
      populate: { path: "course_id" },
    });
    // .populate("favorite", "course_id", populate("course_id","subject"))

    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      res.send({
        statusCode: 200,
        statusText: "success",
        data,
      });
    }
  }
};

const update = async (req, res) => {
  const data = await StudentModel.findOne({ _id: req.user.id });
  const newData = req.body;

  if (!data) {
    res.status(404).send({
      status: 404,
      message: "User id Not Found",
    });
  }

  let imageURL = data.profile_pic;

  if (req.file) {
    //Upload Image File to firebase
    const fileName = new Date().toISOString() + "-" + req.file.originalname;
    imageURL =
      "https://storage.googleapis.com/tutorin-317818.appspot.com/" + fileName;

    const blob = firebase.bucket.file(fileName);
    const blobWriter = blob.createWriteStream({
      metadata: {
        contentType: req.file.mimetype,
      },
    });

    blobWriter.on("error", (err) => {
      if (err) throw err;
    });
    blobWriter.on("finish", (err) => {
      if (err) throw err;
    });
    blobWriter.end(req.file.buffer);
  }

  try {
    newData.profile_pic = imageURL;
    const updatedUser = await StudentModel.findOneAndUpdate(
      { _id: req.user.id },
      newData,
      {
        new: true,
      }
    );

    return res.send({
      statusCode: 200,
      statusText: "success",
      data: updatedUser,
    });
  } catch (err) {
    console.log(err);
    res.status(400).send({
      status: 400,
      message: err.message,
    });
  }
};

const deleteById = async (req, res) => {
  const _id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(_id)) {
    return res.send({
      statusCode: 500,
      statusText: "fail",
      statusMessage: "Format Id invalid",
    });
  } else {
    const data = await StudentModel.findById(_id);
    if (!data) {
      return res.send({
        statusCode: 500,
        statusText: "fail",
        statusMessage: "Id not found",
      });
    } else {
      await StudentModel.findByIdAndRemove(_id);
      return res.send({
        statusCode: 200,
        statusText: "success",
        statusMessage: "Student deleted succesfully",
      });
    }
  }
};

module.exports = {
  register,
  login,
  getAll,
  getStudent,
  update,
  deleteById,
};
