const { TutorModel } = require("../../database/models/tutorModel");
const { CourseModel } = require("../../database/models/courseModel");

exports.search = async (req, res) => {
  //Pagination
  const page = parseInt(req.query.page);
  const limit = parseInt(req.query.limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const data = {};

  const query = {
    $or: [
      {
        category: {
          $regex: req.query.name,
          $options: "i",
        },
      },
      {
        subject: {
          $regex: req.query.name,
          $options: "i",
        },
      },
      {
        tutor_name: {
          $regex: req.query.name,
          $options: "i",
        },
      },
    ],
  };

  if (endIndex < (await CourseModel.countDocuments(query).exec())) {
    data.next = {
      page: page + 1,
      limit: limit,
    };
  }

  if (startIndex > 0) {
    data.previous = {
      page: page - 1,
      limit: limit,
    };
  }
  try {
    data.total_data = await CourseModel.countDocuments(query).exec();
    data.results = await CourseModel.find(query)
      .select(
        "-reviews -__v -createdAt -updatedAt -premium_price -level_id -description"
      )
      .populate("tutor_id", "fullname")
      .limit(limit)
      .skip(startIndex)
      .exec();

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Data is found",
      data,
    };

    if (data.total_data == 0) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Data is not found",
        })
        .status(404);
    } else {
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(404);
  }
};

exports.categoryFilter = async (req, res) => {
  try {
    const getCategoryCourse = await CourseModel.find({
      category: {
        $regex: req.query.name,
        $options: "i",
      },
    })
      .select(
        "-reviews -__v -createdAt -updatedAt -level_id -description -premium_price"
      )
      .populate("tutor_id", "fullname");

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Course is found",
      data: getCategoryCourse,
    };

    if (!getCategoryCourse) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Course is not found",
        })
        .status(404);
    } else {
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(404);
  }
};

exports.subjectFilter = async (req, res) => {
  try {
    const getSubject = await CourseModel.find({
      subject: {
        $regex: req.query.name,
        $options: "i",
      },
    })
      .select(
        "-reviews -__v -createdAt -updatedAt -level_id -description -premium_price"
      )
      .populate("tutor_id", "fullname");

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Course is found",
      data: getSubject,
    };

    if (!getSubject) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Course is not found",
        })
        .status(404);
    } else {
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(404);
  }
};

exports.ascFilter = async (req, res) => {
  try {
    const ascPrice = await CourseModel.find()
      .sort({ regular_price: "asc" })
      .select(
        "-reviews -__v -createdAt -updatedAt -level_id -description -premium_price"
      )
      .populate("tutor_id", "fullname");

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Course is found",
      data: ascPrice,
    };

    if (!ascPrice) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Course is not found",
        })
        .status(404);
    } else {
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(404);
  }
};

exports.descFilter = async (req, res) => {
  try {
    const descPrice = await CourseModel.find()
      .sort({ regular_price: "desc" })
      .select(
        "-reviews -__v -createdAt -updatedAt -level_id -description -premium_price"
      )
      .populate("tutor_id", "fullname");

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Course is found",
      data: descPrice,
    };

    if (!descPrice) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Course is not found",
        })
        .status(404);
    } else {
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(404);
  }
};

exports.levelFilter = async (req, res) => {
  try {
    const getLevel = await CourseModel.find({ level_id: req.params.level_id })
      .select(
        "-reviews -__v -createdAt -updatedAt -level_id -description -premium_price"
      )
      .populate("tutor_id", "fullname");

    const resPayload = {
      statusCode: 200,
      statusText: "success",
      message: "Course is found",
      data: getLevel,
    };

    if (!getLevel) {
      return res
        .json({
          statusCode: 404,
          statusText: "Not Found",
          message: "Course is not found",
        })
        .status(404);
    } else {
      res.json(resPayload).status(200);
    }
  } catch (error) {
    res
      .json({
        statusCode: 500,
        statusText: "error",
        message: error.message,
      })
      .status(404);
  }
};
