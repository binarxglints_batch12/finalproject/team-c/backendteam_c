const admin = require("firebase-admin");
const multer = require("multer");

// Initialize firebase admin SDK
admin.initializeApp({
  credential: admin.credential.cert(
    __dirname +
      "/../../database/config/tutorin-317818-firebase-adminsdk-l2mqy-4e60f09476.json"
  ),
  storageBucket: "tutorin-317818.appspot.com",
});

// Cloud storage
const bucket = admin.storage().bucket();

//Upload Middleware
const upload = multer({
  storage: multer.memoryStorage(),
});

module.exports = {
  bucket,
  upload,
};
