//Require dotenv
require("dotenv").config();

const mongoose = require("mongoose");

const chai = require("chai");
const expect = require("chai").expect;
const chaiHttp = require("chai-http");
const cors = require("cors");
const { app, dbConnection } = require("../server");

//Require DB Connection
// const dbConnection = require("../database/config/index");

const User = require("../database/models/userModel");
const { getMaxListeners } = require("../database/models/userModel");
const UserModel = require("../database/models/userModel");

//configure chai
chai.use(chaiHttp);
chai.should();

const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNjBjZGFmYmFkODNhNmIwMDE1MzRmNjM1IiwibmFtZSI6IkZpdHJhIEhpZGF5YXQiLCJlbWFpbCI6ImZpdHJhaGlkYXlhdEBnbWFpbC5jb20iLCJ1c2VybmFtZSI6ImZpdHJhX2hpZGF5YXQiLCJyb2xlIjoidXNlciJ9LCJpYXQiOjE2MjQxODU5MDksImV4cCI6MTYyNjc3NzkwOX0.qo2Ya56f0baTlZ9MMLYTrRyByHu9hn4abHDzu8mk-q8";

describe("User", () => {
  describe("REGISTRATION", () => {
    //Test REGISTER
    it("Should CREATE User", (done) => {
      chai
        .request(app)
        .post("/api/v1/user/register")
        .send({
          fullName: "User Unit Test",
          userName: "unit_test",
          email: "unit_test@gmail.com",
          password: "unit_test",
        })
        .end((err, res) => {
          // expect(res.text).to.include('{"statusCode": "200}')
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal("Registration successfully");
          expect(res.body.statusText).to.equal("OK");
          expect(res.body.data).to.have.property("id");
          expect(res.body.data).to.have.property("userName", "unit_test");
          expect(res.body.data).to.have.property(
            "email",
            "unit_test@gmail.com"
          );
          done();
        });
    });
  });

  describe("REGISTRATION Using existing email", () => {
    //Test REGISTER using existing email
    it("Should Fail using exist email address", (done) => {
      chai
        .request(app)
        .post("/api/v1/user/register")
        .send({
          fullName: "User Unit Test",
          userName: "unit_test",
          email: "fitrahidayat@gmail.com",
          password: "unit_test",
        })
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(409);
          expect(res.body.message).to.equal(
            "Email Already Exist, please use another email address."
          );
          expect(res.body.statusText).to.equal("Fail");
          done();
        });
    });
  });

  describe("LOGIN", () => {
    //Test to get all user record
    it("should GET user login ", (done) => {
      chai
        .request(app)
        .post("/api/v1/auth")
        .send({
          email: "fitrahidayat@gmail.com",
          password: "fitra_hidayat",
        })
        .end((err, res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal("Login successfully");
          expect(res.body.statusText).to.equal("OK");
          expect(res.body.data).to.have.property("id");
          expect(res.body.data).to.have.property("name");
          expect(res.body.data).to.have.property(
            "email",
            "fitrahidayat@gmail.com"
          );
          expect(res.body.data).to.have.property("userName");
          expect(res.body.data).to.have.property("role");
          expect(res.body.data).to.have.property("token");
          done();
        });
    });
  });
});
